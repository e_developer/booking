class CreateAvailabilities < ActiveRecord::Migration[6.0]
  def change
    create_table :availabilities do |t|
      t.integer :day_of_week
      t.time :start_time
      t.time :end_time
      t.integer :teacher
      t.boolean :available, default: true

      t.timestamps
    end
  end
end
