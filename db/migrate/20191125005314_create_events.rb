class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.text :description
      t.date :start_date
      t.date :end_date
      t.time :hour
      t.integer :user
      t.boolean :available

      t.timestamps
    end
  end
end
