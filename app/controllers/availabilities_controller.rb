class AvailabilitiesController < ApplicationController
  def index
  end

  def new
    @availability = Availability.new
  end

  def create
    @availability = Availability.new(availability_params)

    charge(availability_params)
  end

  def edit
  end

  private

  def charge(availability)
    
    availability["available"].each  do |a|
      av = Availability.new
      av.day_of_week = a[0]
      av.start_time =  a[2,6]
      av.teacher = [*1..10].sample
      av.available = true
      av.save
    end
            # if available.save
            #     flash[:notice] = "Your order was created with success!"
            # else
            #     flash[:alert] = available.errors.full_messages.join(', ')
            # end
  end
    


  def availability_params
     params.require(:availability).permit(available:[])
  end
end
