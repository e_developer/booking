Rails.application.routes.draw do
 
  resources :events
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'pages#index'
  get '/calendar', to: 'calendars#index'

  resources :availabilities
end
